import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


/**
 * This class encrypts/decrypts a file using XOR and brute force.
 * @author Geanina Tambaliuc
 *
 */
public class Cryptomatic {
	
	
	 /**
	  * Encrypting a file using XOR
	 * @param strToEncrypt
	 * @param key
	 * @return
	 */
	private static String encrypt(String strToEncrypt, String key) {
	    	
			//the encrypted string
			StringBuilder str = new StringBuilder();
	        
	        //duplicating the key
			while (key.length() < strToEncrypt.length()) {
		        key=key + key;
		    }
	        
			//converting the strings to array of char
			char[] input=strToEncrypt.toCharArray();
	        char[] input2=key.toCharArray();
	        
	        
	        for(int i = 0; i < input.length; i++) {
	        	
	        	//getting the variables for the XOR operation
	        	String string=String.valueOf(input[i]);
	        	int message = Character.codePointAt(string, 0);
	        	String string2=String.valueOf(input2[i]);
		        int keyXor = Character.codePointAt(string2, 0);
		        
		        //the XOR operation
		        int xor = message ^ keyXor;
		        
		        //converting the XOR value to a hex string
		        String hex= Integer.toHexString(xor);

		        if (hex.length() < 2) 
		        {
		        	hex = "0" + hex;
		        }

		        str.append(hex);
	        }
	        return str.toString();        
	    }

	/**
	 * Decrypting a file using XOR.
	 * @param strToDecrypt
	 * @param key
	 * @return
	 */
	private static String decrypt(String strToDecrypt ,String key) {
		//the decrypted string
	    StringBuilder str = new StringBuilder();
	    
	    //duplicating the key n/2 times
	    while (key.length() < strToDecrypt.length()/2) {
	        key= key + key;
	    }
	    
	    //getting 2 char at a time
	    for (int j=0;j<strToDecrypt.length();j+=2) {
	    	
	    	//getting the values for the XOR operation
	        String hexString = strToDecrypt.substring(j, j+2);
	        int keyXor = key.charAt(j/2);
	        int message = Integer.parseInt(hexString, 16); //converting the hex string to integer
	        
	        //the XOR operation
	        int xor = message ^ keyXor;

	        str.append(Character.toString((char) xor));

	    }
	    return str.toString();
	}
	
	/**
	  * Provides a menu for the user
	 * @return the user's input
	 */
	public static String menu()
	    {
		System.out.println("What do you want to do? ");
		System.out.println("Enter E for XOR encryption.");
		System.out.println("Enter D for XOR decryption.");
		System.out.println("Enter BF for brute force decryption.");
		System.out.println("Enter X to exit the program.");
		System.out.print("Enter your choice: ");
		Scanner aScanner=new Scanner(System.in);
        return aScanner.nextLine();   
	    }
	
	/**
	 * Function to return all the possible keys of 2 characters
	 * @return keys - all the possible keys
	 */
	private static HashSet<String> findKey()
	{
		//set containing all the possible characters from the keyboard
		char[] set= {'`','1','2','3','4','5','6','7','8','9','0','-','=','\\','!','~','@','#','$','%','^','&','*','(',')','_','+','|',
				'q','w','e','r','t','y','u','i','o','p','[',']','{','}','a','s','d','f','g','h','j','k','l',';','"','\'','z','x','c',
				'v','b','n','m',',','.','/','<','>','?','Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L',
				'Z','X','C','V','B','N','M'};
		
		//the length of the set
		int setLength=set.length;
		
		HashSet<String> keys=new HashSet<String>();
		//finding the keys recursively
		return findKeyRecursively(set,setLength,"",2,keys);
		
	}
	
	/**
	 * Finding the keys recursively
	 * @param set - all the possible characters
	 * @param setLength - the length of the set
	 * @param string - the key
	 * @param s - the size of the key
	 * @param keys - the set containing the possible keys
	 * @return keys - all the possible keys
	 */
	private static HashSet<String> findKeyRecursively(char[] set, int setLength, String string, int s, HashSet<String> keys) {
		
		//base case - finding a key and adding it to the set
		if (s==0)
		{
			keys.add(string);
			return keys;
		}
		
		//combine all the characters from the set one by one
		for(int j=0;j<setLength;++j)
		{
			//adding the next character from the set
			String newKey=string+set[j];
			
			//recursive call (decreased the s because I added a new character)
			findKeyRecursively(set,setLength,newKey,s-1,keys);
		}
		return keys;
	}

	/**
	 * The main function.
	 * @param args
	 * @throws FileNotFoundException
	 */
	public static void main(String args[]) throws FileNotFoundException {
		//ask the user what to do
		String choice;
		
		do
		{
			choice=menu();
			
			if(choice.toLowerCase().equals("e"))
			{
				//XOR encryption
					//ask the user for the key
				System.out.println("You chose the XOR Encryption. Please enter the key:  ");
           	 	Scanner s1=new Scanner(System.in);
                String key=s1.nextLine();
                
                	
                	//ask for the filename
                System.out.println("Please enter the name of the file you want to encrypt (include .txt): ");
                Scanner s2=new Scanner(System.in);
                String fileName=s2.nextLine();
                File fileToEncrypt=new File(fileName);
               
                	//check if the file exists
               try
                {	Scanner scanFile=new Scanner(fileToEncrypt);
                	String fileContent="";
                	
                	//read the file
                	while(scanFile.hasNext())
                	{
                		fileContent=fileContent+scanFile.nextLine();
                		fileContent=fileContent+" ";
                	}
                	scanFile.close();
                	
                	//remove the new lines (enters)
					fileContent=fileContent.replaceAll("\\r\\n|\\r|\\n"," ");
					fileContent=fileContent.replaceAll("\\s+", " ");
					
					System.out.println();
					
				
                	//encrypt the file
                	String encrypted = encrypt(fileContent,key);
                	
                	//print the file
                	PrintWriter myWriter=new PrintWriter("encryptedFile.txt");
                	myWriter.print(encrypted);
                	myWriter.close();
                	System.out.println("The file was encrypted. You can find it under the name 'encryptedFile.txt'.");
                }
               catch(Exception e)
               {
            	   System.out.println(e.getMessage());
               }
                
			}
			else if(choice.toLowerCase().equals("d"))
			{
					//XOR decryption
					//ask the user for the key
				System.out.println("You chose the XOR Decryption. Please enter the key (it should be the same key you used for encryption, otherwise the result will not be correct!):  ");
	       	 	Scanner s1=new Scanner(System.in);
	            String key=s1.nextLine();
	          
	            	
	            	//ask for the filename
	            System.out.println("Please enter the name of the file you want to decrypt (include .txt): ");
	            Scanner s2=new Scanner(System.in);
	            String fileName=s2.nextLine();
	            File fileToDecrypt=new File(fileName);
	           
	            	
	            try
	            	{Scanner scanFile=new Scanner(fileToDecrypt);
	            	String fileContent="";
	            	
	            	//read the file
	            	while(scanFile.hasNext())
	            	{
	            		fileContent=fileContent+scanFile.nextLine();
	            	}
	            	scanFile.close();
	            	
	            	//remove the new lines (enters)
					fileContent=fileContent.replaceAll("\\r\\n|\\r|\\n"," ");
					
					//replace any 2 white spaces with one space
					fileContent=fileContent.replaceAll("\\s+", " ");
					
	            	//decrypt the file
	            	String decrypted = decrypt(fileContent,key);
	            	
	            	//print the file
	            	PrintWriter myWriter=new PrintWriter("decryptedFile.txt");
	            	myWriter.print(decrypted);
	            	myWriter.close();
	            	System.out.println("The file was decrypted. You can find it under the name 'decryptedFile.txt'.");
	            	
	            }
	            catch(Exception e)
	            {
	            	System.out.println(e.getMessage());
	            }
				}
			else if(choice.toLowerCase().equals("bf"))
			{
				
				HashSet<String> dictionary = new HashSet();
				File filename=new File("dictionary.txt");
				Scanner readDictionary= new Scanner(filename);
		         
				//reading the dictionary and adding the elements to a hashSet
				while(readDictionary.hasNext())
				{
					dictionary.add(readDictionary.nextLine());
				}
				readDictionary.close();
				
				//ask for the file name
				 System.out.println("Please enter the name of the file you want to decrypt (include .txt): ");
		         Scanner s2=new Scanner(System.in);
		         String fileName=s2.nextLine();
		         File fileToDecrypt=new File(fileName);
		         String fileContent="";
		         
		         try
	            	{
					   Scanner scanFile=new Scanner(fileToDecrypt);
					  
	            	
	            	//read the file you need to decrypt
	            	while(scanFile.hasNext())
	            	{
	            		fileContent=fileContent+scanFile.nextLine();
	            	}
	            	scanFile.close();
	            	}
		         catch(Exception e)
		         {
		        	System.out.println(e.getMessage());
		         }
		         
				//all the possible keys of 2 char
				HashSet<String> keys=findKey();
				
				//used for checking the decryption
				int ok2=0;
				//check each key if it's the right one
				for(String key: keys)
				{
					
					//execute only if a correct decryption has not been found
					if(ok2==0)
					   {
		            	
						//remove the new lines (enters)
						fileContent=fileContent.replaceAll("\\r\\n|\\r|\\n"," ");
						
						//replace any 2 white spaces with one space
						fileContent=fileContent.replaceAll("\\s+", " ");
						
		            	//decrypt the file
		            	String decrypted = decrypt(fileContent,key);
		            	
		            	
		            	//check if the decryption is correct
		            	int ok=checkDecryption(decrypted,dictionary);
		            	
		            	if(ok==1)
		            	{
		            		ok2=1;
		            		System.out.println("A key has been found. Key:" + key);
		            		//print the file
			            	PrintWriter myWriter=new PrintWriter("decryptedFile.txt");
			            	myWriter.print(decrypted);
			            	myWriter.close();
			            	System.out.println("The file was decrypted. You can find it under the name 'decryptedFile.txt'.");
		            	}
		            	}
			           
				}
				if(ok2==0)
				{
					System.out.println("Could not find a key!");
				}
			}
			else if(choice.toLowerCase().equals("x"))
			{
				System.out.println("Exiting the program...");
			}
			else
				System.out.println("Invalid choice. Please reenter!");
		}while(!choice.toLowerCase().equals("x"));
        
   
    }

	/** 
	 * Checking if the decryption is correct
	 * @param decrypted 
	 * @param dictionary
	 * @return
	 */
	private static int checkDecryption(String decrypted, HashSet<String> dictionary) {

		//convert the decryption to an array of char
		char[] array=decrypted.toCharArray();
		
		//checking if the first character is an upper case letter.
		//if it's not then the decryption is incorrect
		if(array[0]<'A' || array[0]>'Z')
			return 0;
		
		for(int i=0;i<array.length;i++)
		{
			//check if after a . ? ! is an uppercase letter
			if(array[i]=='.' || array[i]=='?' || array[i]=='!')
			{
				if(i+1<array.length && i+2<array.length)
				{if(array[i+1]==' ')
				{
					if(array[i+2]<'A' || array[i+2]>'Z')
						return 0;
				}
				
				else if(array[i+1]<'A' || array[i+1]>'Z')
					return 0;
				}
			}
			
			//check if there are two uppercase letters one after another
			if(array[i]>'A' && array[i]<'Z' && i+1<array.length)
			{
				if(array[i+1]>'A' && array[i+1]<'Z')
					return 0;
			}
		}
		
		//check if every word is a accepted English word (from the dictionary)
		
			//split the decryption
			String[] words=decrypted.split(" ");
			
			//checking each word
			for(int i=0;i<words.length;i++)
			{
				//replace any punctuation from the word
				words[i]=words[i].replaceAll("[^\\w\\s]","");
				if(words[i].length()>1)
				{	
					boolean check=isAword(words[i]);
					
				if(!check)
				{
					
					return 0;
				}
				else
				{   
					
					//check if the word is in the dictionary or not
					if(!dictionary.contains(words[i].toLowerCase()))
					{
						return 0;
					}
				}}
			}
		
	    //the decryption is correct and passed all the tests
		return 1;
	}

	/**
	 * Checking if an input is a word. (more than one letter)
	 * @param string 
	 * @return
	 */
	private static boolean isAword(String string) {
		//split the word into chars
		char[] arrayOfChars = string.toCharArray();
		
		if(arrayOfChars.length>1)
		{//check if every char is a letter or not
	    for (char ch : arrayOfChars) {
	        if(!Character.isLetter(ch)) {
	            return false;
	        }
	    }}
	    return true;
	}

   
    
}
